# README #

VTS Nis project

### What is this repository for? ###

Collage project

### How do I get set up? ###

* composer install
* npm install


### API DOCS ###

[Click here for API Docs](https://documenter.getpostman.com/view/1164596/biblioteka/RVtvqDHs)


### Database Image ###

![Scheme](https://image.ibb.co/dh0W0S/Biblioteka_diagram.png)