<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', 'UserController@register');
Route::post('login', 'AuthController@login');

Route::group(['middleware' => ['jwt.auth']], function() {
    Route::get('logout', 'UserController@logout');
    Route::get('self', 'UserController@self');

    // Types
    Route::post('type', 'TypeController@create');
    Route::put('type/{id}', 'TypeController@update');
    Route::delete('type/{id}', 'TypeController@delete');
    Route::get('type', 'TypeController@getAll');
    Route::get('type/{id}', 'TypeController@get');

    // Items
    Route::post('item', 'ItemController@create');
    Route::put('item/{id}', 'ItemController@update');
    Route::delete('item/{id}', 'ItemController@delete');
    Route::get('item', 'ItemController@getAll');
    Route::get('item/{id}', 'ItemController@get');

    // RequestItems
    Route::post('request', 'RequestItemController@create');
    Route::put('request/{id}', 'RequestItemController@update');
    Route::get('request', 'RequestItemController@getAll');
    Route::get('request/{id}', 'RequestItemController@get');
    Route::post('request/{id}', 'RequestItemController@returnItem');
    Route::get('approved', 'RequestItemController@getApproved');

    Route::post('ask/item', 'RequestItemController@requestItem');
    Route::post('return/item', 'RequestItemController@returnItem');
    Route::post('approve/item', 'RequestItemController@approveItem');
});