<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new \App\User();
        $user->name = 'Administrator Demo';
        $user->email = 'admin@demo.com';
        $user->password = bcrypt('123456');
        $user->role = 'ADMIN';
        $user->username = 'admin.demo';
        $user->save();

        $user = new \App\User();
        $user->name = 'User demo';
        $user->email = 'user@demo.com';
        $user->password = bcrypt('123456');
        $user->role = 'USER';
        $user->username = 'user.demo';
        $user->save();
    }
}
