<?php

use Illuminate\Database\Seeder;

class TypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $type = new \App\Type();
        $type->name = 'Knjiga';
        $type->save();

        $type = new \App\Type();
        $type->name = 'Uredjaj';
        $type->save();
    }
}
