<?php

use Illuminate\Database\Seeder;

class ItemTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $item = new \App\Item();
        $item->name = 'Knjiga 1';
        $item->description = 'A sample book';
        $item->type_id = 1;
        $item->status = 1;
        $item->save();

        $item = new \App\Item();
        $item->name = 'Knjiga 2';
        $item->description = 'A sample book';
        $item->type_id = 1;
        $item->status = 1;
        $item->save();

        $item = new \App\Item();
        $item->name = 'Knjiga 3';
        $item->description = 'A sample book';
        $item->type_id = 1;
        $item->status = 1;
        $item->save();

        $item = new \App\Item();
        $item->name = 'Knjiga 4';
        $item->description = 'A sample book';
        $item->type_id = 1;
        $item->status = 1;
        $item->save();
    }
}
