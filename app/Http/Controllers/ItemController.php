<?php

namespace App\Http\Controllers;

use App\Item;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;
use Validator;

class ItemController extends Controller
{
    public function create(Request $request)
    {
        $rules = [
            'name' => 'required',
            'description' => 'required',
            'type_id' => 'required',
            'status' => 'required'
        ];

        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {
            return response()->json(['success'=> false, 'error'=> $validator->messages()]);
        }

        $user = JWTAuth::parseToken()->authenticate();
        if ($user->role == 'ADMIN') {
            $item = new Item();
            $item->fill($request->all());
            if ($item->save()) {
                return response()->json(['success' => true, 'data' => $item], 200);
            }
        }
        return response()->json(['success' => false, 'data' => null], 400);
    }

    public function update(Request $request, $id)
    {
        $user = JWTAuth::parseToken()->authenticate();
        if ($user->role == 'ADMIN') {
            $item = Item::find($id);

            if ($item->update($request->all())) {
                return response()->json(['success' => true, 'data' => $item], 200);
            }
        }
        return response()->json(['success' => false, 'data' => null], 400);
    }

    public function get($id)
    {
        $item = Item::find($id);
        return response()->json(['success' => true, 'data' => $item], 200);
    }

    public function getAll()
    {
        $items = Item::all();

        return response()->json(['success' => true, 'data' => $items], 200);
    }

    public function delete($id)
    {
        $user = JWTAuth::parseToken()->authenticate();
        if ($user->role == 'ADMIN') {
            $item = Item::find($id);
            if ($item->delete()) {
                return response()->json(['success' => true, 'data' => null], 200);
            }
        }
        return response()->json(['success' => false, 'data' => null], 400);
    }
}
