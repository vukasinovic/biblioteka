<?php

namespace App\Http\Controllers;

use App\Item;
use App\RequestItem;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;

class RequestItemController extends Controller
{
    public function create(Request $request)
    {
        $requestItem = new RequestItem();
        $requestItem->fill($request->all());

        if($requestItem->save()) {
            $item = Item::find($request->item_id);

            $item->update(['status' => 0]);

            return response()->json(['success' => true, 'data' => $requestItem], 200);
        }
        return response()->json(['success' => false, 'data' => null], 400);
    }

    public function update(Request $request, $id)
    {
        $requestItem = RequestItem::find($id);


        if($requestItem->update($request->all())) {
            return response()->json(['success' => true, 'data' => $requestItem], 200);
        }
        return response()->json(['success' => false, 'data' => null], 400);
    }

    public function get(RequestItem $requestItem)
    {

        return response()->json(['success' => true, 'data' => $requestItem], 200);
    }

    public function getAll()
    {
        $requestItems = RequestItem::with(['item', 'user'])->where('status', '=', 'PENDING')->get();

        return response()->json(['success' => true, 'data' => $requestItems], 200);
    }

    public function getApproved()
    {
        $requestItems = RequestItem::with(['item', 'user'])->where('status', '=', 'APPROVED')->get();

        return response()->json(['success' => true, 'data' => $requestItems], 200);
    }

    /**
     * Serious work here...
     */
    public function requestItem(Request $request) {
        $item = Item::find($request->item_id);
        $user = JWTAuth::parseToken()->authenticate();

        $requestItem = new RequestItem();
        $requestItem->item_id = $item->id;
        $requestItem->user_id = $user->id;
        $requestItem->status = 'PENDING';

        if($item->status == 1) {
            if($requestItem->save()) {
                if($item->update(['status'=> 0])) {
                    return response()->json(['success' => true, 'data' => $requestItem], 200);
                }
                return response()->json(['success' => false, 'data' => null], 400);
            }
            return response()->json(['success' => false, 'data' => null], 400);
        }

        return response()->json(['success' => false, 'error' => 'already rented.'], 400);
    }



    public function returnItem(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();
        if ($user->role == 'ADMIN') {
            $requestItem = RequestItem::where('item_id', $request->item_id)->with(['user', 'item']);
            $item = Item::find($request->item_id);

            $item->update(['status' => 1]);

            if ($requestItem->delete()) {
                return response()->json(['success' => true, 'data' => $item], 200);
            }
        }
        return response()->json(['success' => false, 'data' => null], 400);
    }

    public function approveItem(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();
        if ($user->role == 'ADMIN') {
            $requestItem = RequestItem::where('item_id', $request->item_id)->with(['user', 'item']);
            $item = Item::find($request->item_id);

            $item->update(['status' => 0]);
            if ($requestItem->update(['status' => 'APPROVED'])) {
                return response()->json(['success' => true, 'data' => $requestItem], 200);
            }
        }
        return response()->json(['success' => false, 'data' => null], 400);
    }
}
