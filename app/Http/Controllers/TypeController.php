<?php

namespace App\Http\Controllers;

use App\Type;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;

class TypeController extends Controller
{
    public function create(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();
        if ($user->role == 'ADMIN') {
            $type = new Type();
            $type->fill($request->all());

            if ($type->save()) {
                return response()->json(['success' => true, 'data' => $type], 200);
            }
        }
        return response()->json(['success' => false, 'data' => null], 400);
    }

    public function update(Request $request, $id)
    {
        $user = JWTAuth::parseToken()->authenticate();
        if ($user->role == 'ADMIN') {
            $type = Type::find($id);


            if ($type->update($request->all())) {
                return response()->json(['success' => true, 'data' => $type], 200);
            }
        }
        return response()->json(['success' => false, 'data' => null], 400);
    }

    public function get(Type $type)
    {

        return response()->json(['success' => true, 'data' => $type], 200);
    }

    public function getAll()
    {
        $types = Type::all();

        return response()->json(['success' => true, 'data' => $types], 200);
    }

    public function delete($id)
    {
        $user = JWTAuth::parseToken()->authenticate();

        if ($user->role == 'ADMIN') {
            $type = Type::find($id);
            if ($type->delete()) {
                return response()->json(['success' => true, 'data' => null], 200);
            }
        }
        return response()->json(['success' => false, 'data' => null], 400);
    }
}
